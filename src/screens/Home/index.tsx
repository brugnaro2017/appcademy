import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';

import styles from './styles';

const Home = (props: {navigation: any; route: any}) => (
  <View style={styles.container}>
    <Image
      style={{height: 100, width: 100}}
      source={{
        uri: 'https://icons-for-free.com/iconfiles/png/512/agenda+app+contacts+online+profile+user+icon-1320183042135412367.png',
      }}
    />
    <TouchableOpacity
      onPress={() => props.navigation.navigate('Lista')}
      style={styles.btnContainer}>
      <Text style={styles.btnColor}>Login</Text>
    </TouchableOpacity>
  </View>
);

export default Home;
